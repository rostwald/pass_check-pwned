# pass_check-pwned

Check passwords in the password-store against the HIBP database.
The plugin uses the range feature of the HIBP API v2 and sends only the first 5 digits of any password hash over the wire.

It is written in pure bourne shell (sh), but pass only accepts extensions with ".bash" filename, hence the confusing file extension.


## Usage

```shell
pass check-pwned [subfolder]
```

It cycles through all entries and returns a warning for any Password entry if the password hash matches an entry in the HIBP database.


## Resources

- [HaveIBeenPwned API documentation](https://haveibeenpwned.com/API/v2#SearchingPwnedPasswordsByRange)
