#!/bin/sh

# Copyright (C) 2018 Sebastian Oswald <sebastian@rostwald.de>. All Rights Reserved
# This file is licensed under the BSD 2-clause license; see LICENSE file for more information.

# This is a simple plugin for password-store, the Standard Unix Password Manager.
# Place in ~/.password-store/.extensions/ or the global extension directory for pass.
# This script is written in pure bourne shell, but pass only accepts extensions with .bash file
# extensions, hence the misleading file extension.


passdir="$PREFIX/$1"

__check() {
	# calculate hash of the password
	pwhash="$(sha1 -qs "$1")"

	# receive list of hashes with matching first 5 chars; grep for second part of the hash and return the counter value normalized as digits
	# see https://haveibeenpwned.com/API/v2#SearchingPwnedPasswordsByRange for API documentation
	count=$(curl -sG https://api.pwnedpasswords.com/range/{`echo "$pwhash" | cut -c 1-5`} | grep -i `echo "$pwhash" | cut -c 6-` | cut -d':' -f2 | tr -cd "[:digit:]")
}

printf "Comparing password hashes with pwnedpasswords database ";

for i in `find $passdir | grep '\.gpg$'`; do
		__check `gpg "${GPG_OPTS[@]}" --quiet -d $i | head -n1`
	if [ "$count" != "" ]; then
		entry=$(echo "$i" | sed "s#$passdir##" | sed "s#\.gpg##")
		printf "\n\nWARNING: Password for $entry has been found $count times in data breaches!\n\n"
	else
		printf "."
	fi
done

printf "\n"
